package com.wallpaper.wallpaper;


import android.app.WallpaperManager;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {


    private int[] images = new int[]{R.drawable.wp1, R.drawable.wp2, R.drawable.wp3, R.drawable.wp4, R.drawable.wp5,
            R.drawable.wp6, R.drawable.wp7, R.drawable.wp8};
    private int imagesCount = images.length;

    @BindView(R.id.button_set)
    Button buttonSet;
    @BindView(R.id.view_pager)
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        initUi();
        makeViewPager();
    }

    private void initUi() {
        ButterKnife.bind(this);
        buttonSet.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WallpaperManager wallpaperManager =
                        WallpaperManager.getInstance(getApplicationContext());

                try {
                    wallpaperManager.setResource(images[pager.getCurrentItem()]);
                    showSnackbar();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });
    }


    private void makeViewPager() {

        PagerAdapter pagerAdapter = new MyFragmentPageAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    private void showSnackbar() {
        Snackbar.make(pager, getString(R.string.wp_installed), Snackbar.LENGTH_SHORT)
                .show();
    }


    private class MyFragmentPageAdapter extends FragmentPagerAdapter {


        public MyFragmentPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return PageFragment.newInstance(images[i]);
        }

        @Override
        public int getCount() {
            return imagesCount;
        }

    }
}


