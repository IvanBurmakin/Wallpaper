package com.wallpaper.wallpaper;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class PageFragment extends Fragment {

    private static final String ARGUMENT_IMAGE_ID = "arg_image_id";
    private int imageId;
    private Unbinder unbinder;
    private View view;

    @BindView(R.id.image_view)
    ImageView imageView;

    static PageFragment newInstance(int image) {

        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_IMAGE_ID, image);
        pageFragment.setArguments(arguments);
        return pageFragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageId = getArguments().getInt(ARGUMENT_IMAGE_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_page, container, false);

        initUi();

        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void initUi() {
        unbinder = ButterKnife.bind(this, view);
        imageView.setImageResource(imageId);
    }

}
